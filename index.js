const num1 = 10;
const num2 = 3

const getCube = num1 ** num2;
console.log(`The cube of ${num1} is ${getCube}`)

const address = ['Laguinding', 'Esperanza', 'Sultan Kudarat']

const [barangay, municipal, province] = address;
console.log(`I lived in ${barangay}, ${municipal}, ${province}`)

const animal = {
	type: 'Dog',
	Name: 'Sky',
	color: 'white',
	genes: 'Pomerian',
	kilogram: 2,
	age: '5 months'
}
function getAnimal({type, Name, color, genes, kilogram, age}){
	console.log(`My ${type} is named ${Name}, He is color ${color}, ${genes}, ${kilogram} Kilos, and ${age} old.`)
}
getAnimal(animal)

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

numbers.forEach((number) =>{
	console.log(number)
	return number;
})


let arrayReduce = numbers.reduce((x, y) => x+y);
console.log(arrayReduce)


class dog{
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new dog('Mica', 2, 'Bitchon Frise');
console.log(myDog)

